// Require express and body-parser
const express = require('express');
const axios = require('axios').default;
const path = require('path')

// Initialize express and define a port
const app = express()
const port = process.env.PORT || 3000

let currentVid = "dQw4w9WgXcQ"

// Tell express to use body-parser's JSON parsing
app.use(express.json())
app.use(express.urlencoded({
	extended: true
}))

// Start express on the defined port
app.listen(port, () => console.log(`Server running on port ${port}`))

app.post("/games", (req, res) => {
	res.status(200).end()
});

app.get("/yt", (req, res) => {
	res.sendFile(path.join(__dirname + '/files/index.html'), (err) => {
		if (err) {
			console.log(err);
		}
	})
	// console.dir(req.params.id)
})

app.get("/check", (req, res) => {
	res.status(200).end()
})

// Post listener - webhook for /message
app.post("/message", (req, res) => {
	const sendMessage = (theMessage) => {
		axios.post('https://api.line.me/v2/bot/message/reply', {
				replyToken: req.body.events[0].replyToken,
				messages: theMessage
			}, {
				headers: {
					Authorization: `Bearer ${process.env.LINE_BOT_CHANNEL_TOKEN}`
				}
			}).then(response => {
				//console.log(response)
				console.log('Reply sent!')
			})
			.catch(error => {
				console.log(JSON.stringify(error.response.data))
			})

		res.status(200).end()
	}

	// Get the message body
	let msgBody = req.body.events[0].type === 'message' ? req.body.events[0].message.text : req.body.events[0].postback.data

	// Check dictionary definitions !define [word]
	if (msgBody.startsWith('!define ')) {
		const theWord = msgBody.slice(8)
		api_oed(theWord)

		// Pick from array at random
	} else if (msgBody.startsWith('!choose ')) {
		const theList = msgBody.slice(8)
		pick_random(theList)
		// Check Japanese dictionary for kanji !j [kanji]
	} else if (msgBody.startsWith('!j ')) {
		const theWord = msgBody.slice(3)
		api_jisho(theWord)
	} else if (msgBody.startsWith('!gsearch ')) {
		const theTerm = msgBody.slice(9)
		api_igdb_search(theTerm)
	} else if (msgBody.startsWith('!gshow ')) {
		const theID = msgBody.slice(7)
		api_igdb_detail(theID)
	}

	// ! Pick at random
	function pick_random(theList) {
		const reDelimiter = /(?:,)\s?/
		let splitString = theList.split(reDelimiter)
		let randomNum = Math.floor(Math.random() * Math.floor(splitString.length))
		for (let i = 0; i < splitString.length; i++) {
			if (i == randomNum) {
				splitString[i] = "→ " + splitString[i]
			} else {
				splitString[i] = "　 " + splitString[i]
			}
		}
		randomThing = splitString.join('\r\n')
		sendMessage(textMessageConstructor(randomThing))
	}

	// ! Call to OED API
	function api_oed(word) {
		axios.get(`https://od-api.oxforddictionaries.com/api/v2/entries/en-gb/${word}`, {
				params: {
					fields: 'definitions',
					strictMatch: false
				},
				headers: {
					app_id: process.env.OED_APP_ID,
					app_key: process.env.OED_APP_KEY
				}
			}).then(response => {
				if (response.data.results) {
					const definitionArray = []
					response.data.results.forEach(result => {
						if (result.lexicalEntries[0].entries[0].senses !== undefined) {
							let definition = {}
							definition.word = result.lexicalEntries[0].text
							definition.type = result.lexicalEntries[0].lexicalCategory.id
							definition.text = result.lexicalEntries[0].entries[0].senses[0].definitions[0]
							definitionArray.push(definition)
						}
					})
					sendMessage(flexMessageConstructor(dicStructure(definitionArray), `Definition for ${word}`))
				}
			})
			.catch(error => {
				if (error.response.data.error) {
					let errorMsg = error.response.data.error
					console.log(errorMsg)
					sendMessage(textMessageConstructor("Sorry, I couldn't find the word you wanted...(´д｀)"))
				} else {
					console.log(error)
				}
			})
	}

	// ! Call to jisho.org API
	function api_jisho(word) {
		return axios.get('https://jisho.org/api/v1/search/words', {
				params: {
					keyword: word,
				}
			}).then(response => {
				if (response.data) {
					const defData = response.data.data[0].senses[0].english_definitions[0]
					// data.data[0].senses.forEach(result => {
					// 	let definition = {}
					// 	definition.english_defs = result.english_definitions
					// 	defArray.push(definition)
					// })
					//dicMessageConstructor(bubbleConstructor(defArray), wordId)
					sendMessage(textMessageConstructor(defData))
				}
			})
			.catch(error => {
				console.log(error)
				return
			})
	}

	// ! Calls to IGDB
	function api_igdb_search(term) {
		return axios({
				url: 'https://api.igdb.com/v4/games/',
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Client-ID': process.env.TW_APP_ID,
					'Authorization': `Bearer ${process.env.TW_APP_ACCESS}`,
				},
				data: `fields name; search "${term}"; where version_parent = null; limit 5;`
			}).then(response => {
				if (response.data) {
					const resultsArray = []
					response.data.forEach(result => {
						resultsArray.push(result)
					})

					sendMessage(flexMessageConstructor(gamesSearchStructure(resultsArray), `Results for: ${term}`))
				}
			})
			.catch(error => {
				console.log(error)
				return
			})
	}

	function api_igdb_detail(id) {
		return axios({
				url: 'https://api.igdb.com/v4/games/',
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Client-ID': process.env.TW_APP_ID,
					'Authorization': `Bearer ${process.env.TW_APP_ACCESS}`,
				},
				data: `fields name, first_release_date, summary, cover.url, url, rating; where id = ${id};`
			}).then(response => {
				if (response.data) {
					sendMessage(flexMessageConstructor(gamesResultStructure(response.data[0]), `Results for: ${response.data[0].name}`))
				}
			})
			.catch(error => {
				console.log(error)
				return
			})
	}
})

// ! Build 'text' message structure
const textMessageConstructor = messageData => {
	const theMessage = [{
		type: 'text',
		text: messageData ? messageData : 'there is no message... :<'
	}]

	return theMessage
}

// ! Build 'flex' message structure
const flexMessageConstructor = (messageData, altText) => {
	const theMessage = [{
		type: 'flex',
		altText: altText,
		contents: messageData
	}]

	return theMessage
}

// ! Dictionary message structure
const dicStructure = definitions => {
	let bubbleContent = {
		"type": "bubble",
		"body": {
			"type": "box",
			"layout": "vertical",
			"contents": [],
			"spacing": "lg",
			"paddingAll": "lg"
		}
	}

	definitions.forEach(entry => {
		bubbleContent.body.contents.push({
			"type": "box",
			"layout": "vertical",
			"contents": [{
					"type": "text",
					"contents": [{
							"type": "span",
							"text": entry.word,
							"weight": "bold",
							"size": "md"
						},
						{
							"type": "span",
							"text": " "
						},
						{
							"type": "span",
							"text": entry.type,
							"size": "xxs"
						}
					]
				},
				{
					"type": "text",
					"text": entry.text,
					"wrap": true,
					"size": "xs",
					"margin": "sm"
				}
			],
			"cornerRadius": "md",
			"paddingAll": "lg",
			"backgroundColor": "#f7f7f7",
			"borderColor": "#eeeeee",
			"borderWidth": "light"
		})
	})

	return bubbleContent
}

// ! Games Search box structure
const gamesSearchStructure = results => {
	let bubbleContent = {
		"type": "bubble",
		"body": {
			"type": "box",
			"layout": "vertical",
			"contents": []
		}
	}
	results.forEach(result => {
		bubbleContent.body.contents.push({
			"type": "button",
			"action": {
				"type": "postback",
				"label": result.name.slice(0, 30),
				"data": `!gshow ${result.id}`
			}
		})
	})
	return bubbleContent
}

const gamesResultStructure = game => {
	let date = new Date(game.first_release_date * 1000)
	let releaseYear = date.getFullYear()
	let coverImg = game.cover ? `https:${game.cover.url}`.replace('t_thumb', 't_cover_big') : null
	let bubbleContent = {
		"type": "bubble",
		"hero": {
			"type": "image",
			"url": coverImg,
			"size": "full",
			"aspectRatio": "20:13",
			"aspectMode": "cover"
		},
		"body": {
			"type": "box",
			"layout": "vertical",
			"contents": [{
					"type": "box",
					"layout": "vertical",
					"contents": [{
						"type": "box",
						"layout": "vertical",
						"contents": [],
						"position": "absolute",
						"backgroundColor": "#03A9F4",
						"height": "10px",
						"width": `${game.rating ? game.rating : 0}%`
					}],
					"height": "10px",
					"backgroundColor": "#f2f2f2"
				},
				{
					"type": "box",
					"layout": "vertical",
					"contents": [{
						"type": "text",
						"text": game.name,
						"size": "lg",
						"weight": "bold"
					}],
					"paddingAll": "lg"
				}
			],
			"paddingAll": "0px"
		}
	}
	if (!coverImg) {
		delete bubbleContent.hero
	}
	if (game.first_release_date) {
		bubbleContent.body.contents[1].contents.push({
			"type": "text",
			"text": releaseYear.toString(),
			"size": "xxs",
			"margin": "xs"
		})
	}
	if (game.summary) {
		bubbleContent.body.contents[1].contents.push({
			"type": "text",
			"text": game.summary.length < 200 ? game.summary : game.summary.slice(0, 200) + '...',
			"wrap": true,
			"size": "sm",
			"margin": "lg"
		})
	}
	if (game.url) {
		bubbleContent.body.contents[1].contents.push({
			"type": "button",
			"action": {
				"type": "uri",
				"label": "Read More",
				"uri": game.url
			},
			"color": "#888888",
			"margin": "xl",
			"style": "primary"
		})
	}
	return bubbleContent
}